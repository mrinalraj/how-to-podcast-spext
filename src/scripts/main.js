$(document).ready(() => {
    $('#ham').click(() => {
        $('#hidden-menu').fadeIn();
    })
    $('#overlay-close').click(() => {
        $('#hidden-menu').fadeOut();
    })
    $(window).scroll(function () {
        var hT = $('#bannerImage').offset().top,
            hH = $('#bannerImage').outerHeight(),
            wH = $(window).height(),
            wS = $(this).scrollTop(),
            wD = wS + wH - 10,
            newsletter = $('.newsletter-widget').offset().top,
            bookmark = $('.bookmark'),
            pOffset = $('.paragraphs').offset();
            maxHeight = document.body.scrollHeight;
        // console.log('ht,hh,wh,ws,wd,yOffset',hT,hH,wH,wS,wD,maxHeight)

        // code to show bookmark after banner, when scrolled down
        if ((hT + hH) < wS + 80 && (maxHeight-wS) >=1000) {
            bookmark.removeClass('u-transition--fadeOut300')
            bookmark.addClass('u-transition--fadeIn300')
        }else{
        // code to hide bookmark when scrolled up
            bookmark.removeClass('u-transition--fadeIn300')
            bookmark.addClass('u-transition--fadeOut300')
        }
        // else if ((maxHeight-wS) <300) {
        //     bookmark.removeClass('u-transition--fadeIn300')
        //     bookmark.addClass('u-transition--fadeOut300')
        // }else if()
        // var post = pOffset.top + $('.paragraphs').outerHeight(),
        //     bookmarkPos = bookmark.offset().top + bookmark.outerHeight()
        // if (bookmarkPos > post) {
        //     bookmark.removeClass('fixed')
        //     bookmark.addClass('absolute')
        //     bookmark.css({
        //         top: post - (hT + hH) - bookmark.outerHeight() - 80 + 'px',
        //         left: -60 + 'px'
        //     })
        // } else if (bookmarkPos < post) {
        //     bookmark.removeClass('absolute')
        //     bookmark.addClass('fixed')
        //     bookmark.css({
        //         top: 150 + 'px',
        //         left: `${pOffset.left - 60}px`
        //     })
        // }
    });

    // $('.bookmark.fixed').css({
    //     left: `${pOffset.left - 50}px`
    // })
})
const linkTo = url => {
    return window.location.href = window.location.href + url
}