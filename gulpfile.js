require('dotenv').config()
const gulp = require('gulp'),
    less = require('gulp-less'),
    pug = require('gulp-pug'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    fs = require('fs'),
    min = require('gulp-minify'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    argv = require('yargs').argv,
    tinypng = require('gulp-tinypng'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber')

let ep = JSON.parse(fs.readFileSync('./src/data/episodes.json').toString()),
    epList = ep.map(e => {
        return {
            name: e.title,
            time: e.readTime,
            des: e.subhead
        }
    })

let getData = () => {
    ep = JSON.parse(fs.readFileSync('./src/data/episodes.json').toString())
    epList = ep.map(e => {
        return {
            name: e.title,
            time: e.readTime,
            des: e.subhead
        }
    })
}

gulp.task('log', () => {
    console.log(epList)
})

gulp.task('less', () => {
    gulp.src('./src/stylesheet/*.less')
        .pipe(plumber())
        .pipe(concat('style.less'))
        .pipe(less())
        .pipe(gulp.dest('./build/css/'))
})

gulp.task('img', () => {
    return gulp.src(['./src/assets/images/**/*.png', './src/assets/images/**/*.jpeg', './src/assets/images/**/*.jpg'])
        .pipe(plumber())
        // .pipe(tinypng(process.env.TINY_API))
        .pipe(gulp.dest('./build/image/'))
})

gulp.task('svg', () => {
    return gulp.src('./src/assets/images/**/*.svg')
        .pipe(plumber())
        .pipe(gulp.dest('./build/image'))
})

gulp.task('watch-img', () => {
    gulp.watch(['./src/assets/images/**/*.png', './src/assets/images/**/*.jpeg', './src/assets/images/**/*.jpg'], ['img'])
    gulp.watch('./src/assets/images/**/*.svg', ['svg'])
})

gulp.task('watch-less', () => {
    gulp.watch('./src/**/*.less', ['less'])
})

gulp.task('serve', () => {
    browserSync.init({
        server: {
            baseDir: './build'
        }
    })
    gulp.watch("./build/**/*.*").on("change", reload);
})

gulp.task('index-pug', () => {
    return gulp.src('./src/view/index.pug')
        .pipe(plumber())
        .pipe(pug({
            locals: {
                title: 'Comprehensive resources for Transcription and Voice Editing',
                epList: epList
            }
        }))
        .pipe(gulp.dest('./build/'))
})

let getUrl = title => title.toLowerCase().replace(/ /g, '-').replace(/,/g, '-').replace(/&/g, '-')

gulp.task('pug', () => {
    ep.forEach((e, i) => {
        gulp.src('./src/view/episodes/template.pug')
            .pipe(plumber())
            .pipe(rename(`index.pug`))
            .pipe(pug({
                locals: {
                    before: {
                        title: i == 0 ? undefined : ep[i - 1].title,
                        link: i == 0 ? undefined : `/episodes/${getUrl(ep[i - 1].title)}/`
                    },
                    after: {
                        title: i == ep.length - 1 ? undefined : ep[i + 1].title,
                        link: i == ep.length - 1 ? undefined : `/episodes/${getUrl(ep[i + 1].title)}/`
                    },
                    title: e.title,
                    chapNum: i + 1,
                    data: e,
                    epList: epList
                }
            }))
            .pipe(gulp.dest(`./build/episodes/${getUrl(e.title)}`))
    })
})

gulp.task('watch-pug', () => {
    gulp.watch(['./src/view/*.pug', './src/view/**/*.pug', '!./src/view/index.pug'], ['pug'])
    gulp.watch('./src/view/index.pug', ['index-pug'])
    gulp.watch('./src/view/partials/**/*.pug', ['pug', 'index-pug'])
})

gulp.task('script', () => {
    return gulp.src('./src/scripts/*.js')
        .pipe(plumber())
        .pipe(concat('bundle.js'))
        .pipe(min())
        .pipe(gulp.dest('./build/js/'))
})

gulp.task('watch-scripts', () => {
    gulp.watch('./src/scripts/*.js', ['script'])
})

gulp.task('reload-data', () => {
    getData()
})

gulp.task('watch-data', () => {
    gulp.watch('./src/data/*.json', ['reload-data', 'build'])
})

gulp.task('clean', () => {
    if (argv.m) return gulp.src('./node_modules').pipe(clean({
        force: true
    }))
    return gulp.src('./build').pipe(clean({
        force: true
    }))
})

gulp.task('install', () => {
    return gulp.src('./package.json').pipe(require('gulp-install'))
})


gulp.task('build', ['less', 'index-pug', 'pug', 'script', 'svg','img'])
gulp.task('watch', ['watch-less', 'watch-pug', 'watch-scripts', 'watch-img', 'watch-data'])
gulp.task('default', ['build', 'watch', 'serve'])